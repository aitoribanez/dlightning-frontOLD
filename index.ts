import fs from "fs";
import handlebars from "handlebars";

let data = {
    bitcoin: {
        template: fs.readFileSync("./templates/bitcoin-template.conf", "utf8"),
        dest: "./bitcoin.conf"
    },
    ln: {
        // template: fs.readFileSync("./templates/ln-template.conf", "utf8"),
        dest: "./ln.conf"
    }
}

function btcConfig() {

	let source = {
        rpcuser : "rpcuser",
        rpcpass : "rpcpass"
	};

    let btcConfigBuilder = handlebars.compile(data.bitcoin.template);
    let btcConfig = btcConfigBuilder(source);

    fs.writeFileSync(data.bitcoin.dest, btcConfig)

}

btcConfig();