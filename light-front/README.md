Frontend part of the portal.

- Using vuejs.

https://vuejsdevelopers.com/2017/04/22/vue-js-libraries-plugins/

# Run
```npm run serve```, open http://localhost:8080 on your browser.

# Build

```npm run build```
