= Frontend for dLiggtning server =

De momento, teniendo unos datos (un objeto), saca el fichero de ```bitcoin.conf``` correspondiente basandose en la plantilla ```templates/bitcoin-template.conf```. 
Adaptado a ```ln.conf``` tambien.

Esos datos tienen que venir de una webapp (vuejs probablemente), haciendo una petición POST al servidor http que haya en el nodo/servidor (se realizará con express).

Probado en nodejs-v10.5.0, la última version estable. La 8.11.3 es la LTS. +
Para instalar la versión concreta de node, usar nvm https://github.com/creationix/nvm

- Ejecucion.

- git clone xxxx
- cd dlightning-front
- npm install
- Cambiar algo en el objeto de ```index.ts```. Por ejemplo el ```rpcuser``` que esta entrecomillas (""), cambiarlo a ```rpcuser111```
- npm run buildExec
- Mirar el contenido de ```./bitcoin.conf```

= Run http server =
```node server.js```
